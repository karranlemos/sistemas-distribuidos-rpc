import sys
import uuid
import time
from datetime import datetime
from xmlrpc.client import ServerProxy
from requests.exceptions import RequestException

from libs.classes_utils import ClientMethods
from classes.xmlrpc_server import CustomXMLRPCServer, RPCServerRunner
from classes.utils import OptionsRunner, CommandOption
from classes.socket_utils import get_free_port
from classes.json_utils import open_json_env

env = open_json_env('.env.json', { 'BINDER_PORT': 3000 })

BINDER_PORT = env['BINDER_PORT']
BINDER_ADDRESS = f'http://localhost:{BINDER_PORT}'
PORT: int = get_free_port()
CLIENT_ID = str(uuid.uuid1())

def main():
    print(f'Port number: {PORT}')

    try:
        with ServerProxy(BINDER_ADDRESS) as binder:
            binder.register_client(CLIENT_ID, PORT)
    except:
        print('Failed to bind name to naming server.')
        print('Quitting...')
        sys.exit(1)

    client_methods = ClientMethods()

    server = CustomXMLRPCServer(('localhost', PORT), allow_none=True, logRequests=False)
    server.register_instance(client_methods)

    rpc_server = RPCServerRunner(server)
    rpc_server.start()

    run_input(server, client_methods)

def run_input(server: CustomXMLRPCServer, client_methods: ClientMethods):
    class QuitOption(CommandOption):
        def run_option(self):
            server.shutdown()
            try:
                with ServerProxy(BINDER_ADDRESS) as binder:
                    binder.unregister_client(PORT)
            except:
                print('An error occurred while retrieving server data.')
            sys.exit()
        
        def get_help(self) -> str or None:
            return f"{self.options}: Quit program."

    class DisplayMessagesOption(CommandOption):
        def run_option(self):
            message_number = 0
            print('\n-----------\n')

            for message in client_methods.messages:
                message_number += 1
                formatted_date = datetime \
                    .fromtimestamp(message.timestamp) \
                    .strftime('%d/%m/%Y %H:%M:%S')

                from_client = message.client_name
                if message.client_name == CLIENT_ID:
                    from_client = f'(Myself) {from_client}'
                    
                print('#', message_number)
                print('From:', from_client)
                print('At:', formatted_date)
                print('Message:', message.message)
                print()

            print('\n-----------\n')
        
        def get_help(self) -> str or None:
            return f"{self.options}: Display messages."

    class SendMessage(CommandOption):
        def run_option(self):
            message = input('message> ')

            with ServerProxy(BINDER_ADDRESS) as binder:
                try:
                    server_data = binder.get_server()
                except:
                    print('An error occurred while retrieving server data.')
                    return
            
            if server_data is None:
                print('No server connected.')
                return
            if not isinstance(server_data, dict):
                print('Corrupted server data retrieved')
                return
            if not 'port' in server_data:
                print('Server port not found.')
                return

            server_port = server_data.get('port')
            
            with ServerProxy(f'http://localhost:{server_port}') as server:
                print('"sending" to server...')
                try:
                    server.distribute_messages({
                        'client_name': CLIENT_ID,
                        'timestamp': time.time(),
                        'message': message,
                    })
                except Exception as e:
                    print('An error occurred while sending message.')
                    print(e)
                    return

            print('Done!')

        def get_help(self) -> str or None:
            return f"{self.options}: Send message."

    try:    
        OptionsRunner([
            QuitOption('q', 'Q'),
            SendMessage('m', 'M'),
            DisplayMessagesOption('d', 'D')
        ]).run()
    except KeyboardInterrupt:
        print('Keyboard Interruption detected.')
    except Exception:
        print('Unknown error...')
    finally:
        print('Quitting...')
        QuitOption().run_option()

if __name__ == '__main__':
    main()