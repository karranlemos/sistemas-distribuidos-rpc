from typing import List, Dict

from classes.messaging import MessageBody

class ClientMethods:
    messages: List[MessageBody] = []

    def retrieve_message(self, message: Dict):
        message_body = MessageBody(
            client_name=message['client_name'],
            timestamp=message['timestamp'],
            message=message['message'],
        )

        self.messages.append(message_body)