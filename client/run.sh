FOLDER=$(dirname $0)

export PYTHONPATH=$PYTHONPATH:$(readlink -f "$FOLDER/../libs")
echo $PYTHONPATH

python3 "$FOLDER/main.py"