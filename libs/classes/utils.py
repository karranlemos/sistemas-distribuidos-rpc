from abc import ABC, abstractmethod
from typing import List

class CommandOption(ABC):
    def __init__(self, *options: str):
        self.options: List[str] = list(options)

    @abstractmethod
    def run_option(self):
        pass

    def match_option(self, *options: str) -> bool:
        for option in options:
            if option in self.options:
                return True
        return False


    def get_help(self) -> str or None:
        return None

class OptionsRunner:
    def __init__(
        self,
        command_options: List[CommandOption],
        prompt: str = '>',
        help_keys: List[str] = ['h', 'H'],
    ):
        self.help_option = self._create_help_option(help_keys)
        self.command_options = [self.help_option] + list(command_options)
        self.prompt = prompt

    def run(self):
        while True:
            command = input(f'\n{self.prompt} ')
            command_option = self._find_match(command)

            if command_option is None:
                self._print_help()
                continue

            command_option.run_option()
            
    def _find_match(self, command) -> CommandOption or None:
        for command_option in self.command_options:
            if not command_option.match_option(command):
                continue

            return command_option

        return None

    def _print_help(self):
        print('Help')
        for command_option in self.command_options:
            command_help = command_option.get_help()
            if command_help is not None:
                print(command_help)

    def _create_help_option(self, keys: List[str]) -> CommandOption:
        print_help = self._print_help
        class HelpOption(CommandOption):
            def run_option(self):
                print_help()
            def get_help(self) -> str or None:
                return f'{self.options}: Prints help.'

        return HelpOption(*keys)
