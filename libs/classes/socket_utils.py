import socket

def get_free_port():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('', 0))
        address = s.getsockname()
        port: int = address[1]
        s.close()
        
        return port
    except:
        raise Exception('Failed to retrieve port.')