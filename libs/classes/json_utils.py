import sys
import json
from typing import Dict

def open_json_env(path: str, defaults: Dict):
    env = dict()
    env.update(defaults)
    
    try:
        with open(path) as envfile:
            data = json.load(envfile)
    except:
        print('Error reading env file. Picking default values...')
        return env

    env.update(data)
    return env

