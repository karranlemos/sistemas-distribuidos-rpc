import sys
from threading import Thread

from xmlrpc.server import SimpleXMLRPCServer

class CustomXMLRPCServer(SimpleXMLRPCServer):
    def process_request(self, request, client_address):
        self.client_address = client_address
        self.request = request
        return SimpleXMLRPCServer.process_request(
            self, request, client_address
        )

class RPCServerRunner(Thread):
    def __init__(self, server: SimpleXMLRPCServer):
        Thread.__init__(self)
        self.server = server

    def run(self):
        try:
            print('Serving...')
            self.server.serve_forever()
        except:
            print('Error detected... quitting...')
            sys.exit(1)