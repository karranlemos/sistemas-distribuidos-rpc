from dataclasses import dataclass
from typing import Dict

@dataclass
class MessageBody:
    client_name: str
    timestamp: float
    message: str

@dataclass
class MulticastMessageBody(MessageBody):
    server_name: str