import sys

from libs.classes_utils import Binder
from classes.xmlrpc_server import CustomXMLRPCServer
from classes.json_utils import open_json_env

env = open_json_env('.env.json', { 'PORT': 3000 })
PORT = env['PORT']

server = CustomXMLRPCServer(('localhost', PORT), allow_none=True)
server.register_instance(Binder())

try:
    print(f'Serving on port {PORT}...')
    server.serve_forever()
except KeyboardInterrupt:
    print('Ending program...')
except:
    print('Error detected... quitting...')