from dataclasses import dataclass
from datetime import datetime
from typing import Dict

@dataclass
class Client:
    name: str
    port: int

class Binder:
    def __init__(self):
        self.registered_server: Client or None = None
        self.clients: Dict[int, Client] = {}

    def register_client(self, name, server_port: int):
        self.clients[server_port] = Client(
            name=name,
            port=server_port,
        )
        print(f'Client of name "{name}" registered...')

    def unregister_client(self, port):
        if port not in self.clients:
            return

        del self.clients[port]
        print(f'Client on port "{port}" unregistered...')

    def register_server(self, name: str, server_port: int):
        if self.registered_server and self.registered_server.name == name:
            '''Inform server'''

        self.registered_server = Client(name, server_port)
        print(f'Server of name "{name}" registered...')

    def unregister_server(self, port: int):
        if not self.registered_server:
            raise ValueError('No server registered.')
        if self.registered_server.port != port:
            raise ValueError('Wrong server port.')
        
        self.registered_server = None
        print(f'Server of port "{port}" unregistered...')
    
    def report_unresponsive(self, port: str):
        '''Check if client is still responsive. If not, it's removed.'''

    def get_clients(self) -> Dict:
        formatted_clients = []
        for client in self.clients.values():
            formatted_clients.append({
                'name': client.name,
                'port': client.port,
            })
        return formatted_clients

    def get_server(self) -> Dict or None:
        if not self.registered_server:
            return None

        return {
            'name': self.registered_server.name,
            'port': self.registered_server.port,
        }