import sys
import uuid
from typing import Dict
from xmlrpc.client import ServerProxy

from libs.classes_utils import ServerMethods
from classes.xmlrpc_server import CustomXMLRPCServer
from libs.udp_broadcast import UDPBroadcaster, UDPSender
from libs.messages import RPCMessagesDistributor, MulticastMessagesDistributor
from classes.socket_utils import get_free_port
from classes.json_utils import open_json_env

env = open_json_env('.env.json', {
    'BINDER_PORT': 3000,
    'UDP_PORT': 3001,
    'MULTICAST_IP': '230.0.0.1',
})

MULTICAST_IP = env['MULTICAST_IP']
BINDER_PORT = env['BINDER_PORT']
UDP_PORT = env['UDP_PORT']
CLIENT_ID = str(uuid.uuid1())
PORT: int = get_free_port()

print('UDP_PORT', UDP_PORT)

class CustomBroadcaster(UDPBroadcaster):
    def __init__(self):
        print(f'Port number: {PORT}')

        UDPBroadcaster.__init__(self, MULTICAST_IP, UDP_PORT)
        self.messages_distributor = RPCMessagesDistributor(BINDER_PORT, CLIENT_ID)

    def do_on_receive(self, data: Dict[str, str or int or float], address: str):
        print('Message received via musticast. Distributing via RPC.')
        self.messages_distributor.distribute_message(data)

def main():
    try:
        with ServerProxy(f'http://localhost:{BINDER_PORT}') as binder:
            binder.register_server(CLIENT_ID, PORT)
    except:
        print('Failed to bind name to naming server.')
        print('Quitting...')
        sys.exit(1)

    udp_broadcaster = CustomBroadcaster()
    udp_broadcaster.start()

    server = CustomXMLRPCServer(('localhost', PORT), allow_none=True)
    server.register_instance(
        ServerMethods(
            CLIENT_ID,
            MulticastMessagesDistributor(
                client_id=CLIENT_ID,
                udp_sender=UDPSender(udp_broadcaster)
            )
        )
    )

    try:
        print('Serving...')
        server.serve_forever()
    except KeyboardInterrupt:
        print('Quitting...')
    except:
        print('Error detected... quitting...')
    finally:
        try:
            udp_broadcaster.close()
            binder.unregister_server(PORT)
        except:
            print('Error unbinding connections.')

if __name__ == '__main__':
    main()