from abc import ABC, abstractmethod
from datetime import time
from typing import Dict, List, Any
from xmlrpc import client
from xmlrpc.client import ServerProxy
from threading import Thread

from classes.messaging import MessageBody
from .udp_broadcast import UDPSender

class MessagesDistributor(ABC):
    @abstractmethod
    def distribute_message(self, message: Dict[str, Any]):
        pass

class RPCMessagesDistributor(MessagesDistributor):
    def __init__(self, binder_port: int, client_id: str):
        self.binder_port = binder_port
        self.client_id = client_id

    def distribute_message(self, message: Dict[str, Any]):
        message_body = MessageBody(
            client_name=message['client_name'],
            timestamp=message['timestamp'],
            message=message['message'],
        )
        
        try:
            clients = self._retrieve_clients()
        except:
            print('An error occurred while retrieving server data.')
            return

        self._send_messages_to_all_client(message_body, clients)

    def _retrieve_clients(self) -> Dict[str, str or int]:
        with ServerProxy(f'http://localhost:{self.binder_port}') as binder:
            binder.set
            return binder.get_clients()

    def _send_messages_to_all_client(
        self,
        message_body: MessageBody,
        clients: Dict[str, str or int]
    ):
        threads: List[Thread] = []
        for client in clients:
            threads.append(Thread(target=self._send_message_to_client, args=(message_body, client['port'],)))

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

    def _send_message_to_client(self, message_body: MessageBody, port: int):
        with ServerProxy(f'http://localhost:{port}') as client_server:
            print(f'sending to client on port {port}...')
            try:
                client_server.retrieve_message({
                    'client_name': message_body.client_name,
                    'timestamp': message_body.timestamp,
                    'message': message_body.message,
                })
            except Exception as e:
                print(f'An error occurred while sending message to client on port {port}.')
                print(e)
                return

class MulticastMessagesDistributor(MessagesDistributor):
    def __init__(self, client_id: int, udp_sender: UDPSender):
        self.client_id = client_id
        self.udp_sender = udp_sender

    def distribute_message(self, message: Dict[str, Any]):
        self.udp_sender.send({ 'server_name': self.client_id, **message })