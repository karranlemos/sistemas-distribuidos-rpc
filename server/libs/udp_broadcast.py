import socket
import struct
import json
import time
from abc import ABC, abstractmethod
from typing import Dict
from threading import Thread

MESSAGE_BUFFER = 1024
MULTICAST_TTL = 2

class UDPBroadcaster(Thread, ABC):
    def __init__(self, ip: str, port: int):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.sock = socket.socket(
            socket.AF_INET,
            socket.SOCK_DGRAM,
            socket.IPPROTO_UDP
        )
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, MULTICAST_TTL)
        self.sock.bind((self.ip, self.port))
        mreq = struct.pack("4sl", socket.inet_aton(self.ip), socket.INADDR_ANY)
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        
        self.sock.settimeout(2)

        self.is_closed = True

    def send(self, message: Dict):
        bytes_message = bytes(json.dumps(message), encoding='utf-8')
        self.sock.sendto(bytes_message, (self.ip, self.port))

    def run(self):
        self.is_closed = False
        while not self.is_closed:
            try:
                data, address = self.sock.recvfrom(MESSAGE_BUFFER)
                data = data.decode('utf-8')
                data = json.loads(data)

                if ('shutdown' in data and data['shutdown'] == True):
                    return

                self.do_on_receive(data, address)
            except socket.timeout:
                continue
            except:
                print('Error on receiving broadcasting...')

    def close(self):
        self.is_closed = True

    @abstractmethod
    def do_on_receive(self, data: Dict[str, str or int or float], address: str):
        pass

class UDPSender:
    def __init__(self, udp_subscriber: UDPBroadcaster):
        self._udp_subscriber = udp_subscriber

    def send(self, message: Dict):
        self._udp_subscriber.send(message)

if __name__ == '__main__':
    class CustomBroadcaster(UDPBroadcaster):
        def __init__(self):
            UDPBroadcaster.__init__(self, '230.0.0.7', 9907)

        def do_on_receive(self, data: Dict[str, str or int or float], address: str):
            print(data)

    udp = CustomBroadcaster()
    udp.start()
    udp.send({ 'sadsad': 1 })
    # udp.close()
    print('End. :)')