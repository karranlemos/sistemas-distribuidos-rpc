from typing import Dict

from .messages import MessagesDistributor

class ServerMethods:
    def __init__(self, client_id: int, messages_distributor: MessagesDistributor):
        self.messages_distributor = messages_distributor
        self.client_id = client_id

    def distribute_messages(self, message: Dict):
        message = {
            'server_name': self.client_id,
            'client_name': message['client_name'],
            'timestamp': message['timestamp'],
            'message': message['message']
        }

        try:
            print('Multicasting message via UDP.')
            self.messages_distributor.distribute_message(message)
        except:
            print('An error occurred while retrieving server data.')
            return